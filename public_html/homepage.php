<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Private Jet Charter</title>
  <link rel="stylesheet" type="text/css" href="/resources/css/pjc.css">
</head>
<body>
  <div id="wrapper">

    <div id="header">
      <h1>Private Jet Charter</h1>
    </div>

    <div class="container leader">
      <div class="lead-copy">
        <h2>Leasing Private Aircraft for Over 20 Years</h2>

        <p>People in the know have relied on Private Jet Charter to provide the highest quality private aircraft hire services for more than 20 years. Be it Jet hire for business meetings, aircraft rental for corporate events, flying private for entertainment purposes or to lease a jet/helicopter for personal travel. Quite simply, we have the experience and ability to make sure you get where you need to be, when you need to be there - in luxury, style and comfort.</p>
      </div>

      <img class="lead-image" src="http://placekitten.com/g/640/300">
    </div>

    <div class="container modules">
      <div id="module-corporate">
        <h2>Hiring a Corporate Airliner</h2>

        <p>If a large group is taking a trip, chartering private jets is the ideal way of travelling together without the rigours of usual air travel. Sports clubs, corporate events managers and conference organisers may be pleasantly surprised by the cost per head.</p>

        <a class="btn" href="#">Find out more &raquo;</a>
      </div>

      <div id="module-service">
        <h2>Putting the Customer First</h2>

        <p>We always strive to make your trip extra special and totally personal to you. Birthdays, gourmet foods, VIP services, even favourite crew, are just part of the service. So, if there is anything you want with your jet hire – just ask.</p>

        <a class="btn" href="#">Find out more &raquo;</a>
      </div>
    </div>

    <div id="footer">

    </div>

  </div>
</body>
</html>
