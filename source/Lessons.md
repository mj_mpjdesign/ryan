# Ryan's Training

## Lesson 1: Make it 'Pop'

You've been asked to make some changes to a website to bring out the most important pieces of information in the website's lead paragraph, and make them more obvious.

Another developer has modified the HTML, wrapping the important pieces of information in `<span>` tags. It's your job to finish up what he started.

### 1.1: Make the important information appear bold without touching the HTML

```css
span {
  font-weight: bold;
}
```

That's great, but you've created a small problem: all `<span>` elements on the whole page have turned bold, breaking some other styles. We only want your changes to apply to the top paragraph.

We can see from the HTML (either using the web inspector or looking at the PHP file itself) that the top area is wrapped in a `<div class="jumbotron">` - can we use this to target our changes more accurately?

### 1.2: Only affect a certain part of the page

```css
.jumbotron span {
  font-weight: bold;
}
```

Much better - though the client wants to use some colour to pick out the where and when of the event. It looks like the other developer has marked up those parts with classes already.

### 1.3: Overriding with classes

```css
span.meetup-info {
  color: #5cb85c;
}
```

Great! One final change to that top paragraph: the word "drinks" is in bold too, but the client doesn't want that. Drinks are still important though, so we should colour it in instead of making it bold. Let's make it pink.

### 1.4: Adding a class

We already have a bit of pink text at the bottom right of the page, that's in its own classed span:

```html
…<span class="callout">#digibury</span>…
```

We can just take that class and add it to to the "drinks" `<span>`:

```html
<span class="callout">drinks</span>
```

We're halfway there - this adds the colour to the drinks class, but it's still bold.

### 1.5: Multiple classes

We can use more than one class to add their combined effects to an element. Let's add a second class to our drinks `<span>`:
```html
<span class="callout meetup-beverages">drinks</span>
```

Now we can target it:

```css
span.meetup-beverages {
  font-weight: normal;
}
```

We don't need to redefine the colour, as the `.callout` class does that for us.


## Lesson 2: Fixing inline styles

A new block of text has been added to the page, but it uses the now-deprecated `<font>` tag. Using what you've learnt, remove all inline styles and font tags without changing the look of the page.

You should end up with something like:

```css
span.pjc-orange {
  color: #5cb85c;
}
```

And reusing the `.callout` class for the client.
