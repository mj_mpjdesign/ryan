# CSS training

There are several branches in this repo:

1. `master`: Basic branch for beginning work on
2. `highlights`: What you should have after completing Lesson 1 in Lessons.pdf
3. `inline-styles`: Branch for beginning Lesson 2 with
4. `ryan`: Branch for Ryan to work on

This will work best running two web servers. The student can use one server running the `ryan` branch, while the instructor can switch between the other three to show the desired results.

## The 'homepage' exercise

`homepage.php` is an example site that has no conflicting styles on it (no bootstrap, normalize, foundation etc). The student's job would be to style it according to the homepage.pdf file (though creative license may be taken!)
